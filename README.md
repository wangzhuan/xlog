# xlog

## Features

1. 封装zap日志打印作为file和console的打印系统
2. 只需实现`Handler`接口，可以自由选择将日志打印到不同的系统（本地、远程）
3. 可选系统默认实例或自己组装不同的`Handler`形成不同的`logger`实例，进行不同需求的输出

#### 名词说明：

###### Handler

实现了以下方法的接口：

```go
// Handler is used to handle log events, outputting them to
// stdio or sending them to remote services.
type Handler interface {
	Log(context.Context, Level, string, ...interface{})
	Close() error
}
```

###### logger

组合了多个`Handler`的一个日志打印实例

```go
// 结构体
type XLogger struct {
	h *Handlers
}

// 系统默认初始化一个fileHandler，注册成logger
func Init(c *Config, serviceName string) {
	if c == nil {
		fileConfig := &FileConfig{
			Console:true,
		}
		c = &Config{FileConfig:fileConfig}
	}
	fileHandler := NewFileHandler(c.FileConfig, serviceName)
	xLogger.h = NewHandlers(fileHandler)
}

// 系统提供的一系列方法
func Debug(format string, args ...interface{})

func Info(format string, args ...interface{})

func Warn(format string, args ...interface{})

func Error(format string, args ...interface{})

func Panic(format string, args ...interface{})

func Fatal(format string, args ...interface{})

// 要想打印trace id，需要传入*gin.Context，框架会自动打印
func DebugC(ctx context.Context, format string, args ...interface{})

func InfoC(ctx context.Context, format string, args ...interface{})

func WarnC(ctx context.Context, format string, args ...interface{})

func ErrorC(ctx context.Context, format string, args ...interface{})

func PanicC(ctx context.Context, format string, args ...interface{})

func FatalC(ctx context.Context, format string, args ...interface{})
```

###### Config配置

```go
type Config struct {
	FileConfig *FileConfig
}
type FileConfig struct {
	LogFilePath string // 日志路径
	MaxSize     int    // 单个日志最大的文件大小. 单位: MB
	MaxBackups  int    // 日志文件最多保存多少个备份
	MaxAge      int    // 文件最多保存多少天
	Compress    bool   // 是否压缩备份文件
	Console     bool   // 是否命令行输出，开发环境可以使用
	LevelString string // 输出的日志级别, 值：debug,info,warn,error,panic,fatal
}
```

###### 打印日志的含义

```json
{"level":"info","time":"2019-05-29T11:02:31+08:00","msg":"this is info msg, hello world","path":"ll.go:11(ll.LogTest)"}
// level：日志级别
// time：时间
// msg：实际的消息
// path：打点位置：文件名:行号(包名.函数名)
```



## 使用方法:

### 使用系统内部函数（日常默认使用这个就行）

事例：

```go
func LogTest() {
	xlog.Init(nil, "")
	xlog.Info("this is info msg, hello %s", "world")
	xlog.Error("this is info msg, hello %s", "world")
	xlog.Debug("this is info msg, hello %s", "world")
	xlog.Warn("this is info msg, hello %s", "world")
}
```

打印如下：

```json
{"level":"info","time":"20190522204104","msg":"this is info msg, hello world","path":"ll.go:11(ll.LogTest)"}
{"level":"error","time":"20190522204104","msg":"this is error msg, hello world","path":"ll.go:12(ll.LogTest)"}
{"level":"debug","time":"20190522204104","msg":"this is debug msg, hello world","path":"ll.go:13(ll.LogTest)"}
{"level":"warn","time":"20190522204104","msg":"this is warn msg, hello world","path":"ll.go:14(ll.LogTest)"}
```

### 扩展用法（一个项目多个不同定义输出，输出到远程等）

事例：

```go
func LogManyTest() {
	loggerA := xlog.NewLogger(nil, "")
	loggerA.Info("this is (A) info msg, hello %s", "world")
	loggerA.Debug("this is (A) debug msg, hello %s", "world")
    
    
	loggerB := xlog.NewLogger(&xlog.Config{xlog.FileConfig:&FileConfig{LogFilePath:"./testpath/"}}, "serviceName")
	loggerB.Debug("this is (B) debug msg, hello %s", "world")
}
```

打印结果：

```json
{"level":"info","time":"20190522204553","msg":"this is (A) info msg, hello world","path":"ll.go:23(ll.LogManyTest)"}
{"level":"debug","time":"20190522204553","msg":"this is (B) debug msg, hello world","path":"ll.go:25(ll.LogManyTest)"}
```

# 目前接入Kafka配置的项目如下
- [xng-system-message](https://xgit.xiaoniangao.cn/xngo/xng/xng_system_message)
