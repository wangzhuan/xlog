package xlog

import "github.com/segmentio/kafka-go"

type Logger interface {
	kafka.Logger
}

type kafkaSinkLogger struct {
	xLogger *XLogger
}

func (k kafkaSinkLogger) Printf(s string, i ...interface{}) {
	k.xLogger.InfoS(1, s, i...)
}

func NewKafkaSinkLogger() Logger {
	c := &FileConfig{
		LogFilePath:   "./xlog.log",
		MaxSize:       100,
		MaxBackups:    2,
		MaxAge:        1,
		Console:       false,
		LevelString:   "info",
		RotationTime:  0,
		FilePathDepth: 0,
	}
	xLogger = NewLogger(&Config{FileConfig: c}, "")
	return kafkaSinkLogger{xLogger: xLogger}
}
