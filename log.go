package xlog

import (
	"context"
	"fmt"
)

type XLogger struct {
	h *Handlers
}

type Config struct {
	FileConfig *FileConfig `yaml:"fileConfig"`
}

var (
	xLogger *XLogger
)

const _defaultSkip = 0

func InitWithKafkaConfig(kafkaConfig *KafkaConfig) error {
	xLogger1, err := NewWithKafkaConfig(kafkaConfig)
	if err != nil {
		return err
	}
	xLogger = xLogger1
	return nil
}

func Init(c *Config, serviceName string) {
	if c == nil {
		fileConfig := &FileConfig{
			Console: true,
		}
		c = &Config{FileConfig: fileConfig}
	}
	fileHandler := NewFileHandler(c.FileConfig, serviceName)
	xLogger = &XLogger{}
	xLogger.h = NewHandlers(fileHandler)
}

func NewWithKafkaConfig(kafkaConfig *KafkaConfig) (*XLogger, error) {
	fmt.Printf("\n--> start NewWithKafkaConfig... \n")
	handler, err := NewKafkaHandler(kafkaConfig)
	if err != nil {
		fmt.Printf("--> fail to NewWithKafkaConfig..., err: %v \n", err)
		return nil, err
	}
	logger := &XLogger{}
	logger.h = NewHandlers(handler)
	fmt.Printf("--> end NewWithKafkaConfig. \n")
	return logger, nil
}

func NewLogger(c *Config, serviceName string) *XLogger {
	var xLogger = &XLogger{}
	if c == nil {
		fileConfig := &FileConfig{
			Console: true,
		}
		c = &Config{FileConfig: fileConfig}
	}
	fileHandler := NewFileHandler(c.FileConfig, serviceName)
	xLogger.SetHandlers(NewHandlers(fileHandler))
	return xLogger
}

func (xLogger *XLogger) SetHandlers(hs *Handlers) {
	xLogger.h = hs
}

func GetDefaultLogger() *XLogger {
	return xLogger
}

func DebugC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, DebugLevel, _defaultSkip, format, args...)
}

func InfoC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, InfoLevel, _defaultSkip, format, args...)
}

func WarnC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, WarnLevel, _defaultSkip, format, args...)
}

func ErrorC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, ErrorLevel, _defaultSkip, format, args...)
}

func PanicC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, PanicLevel, _defaultSkip, format, args...)
}

func FatalC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, FatalLevel, _defaultSkip, format, args...)
}

func DebugW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), DebugLevel, _defaultSkip, msg, m)
}

func InfoW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), InfoLevel, _defaultSkip, msg, m)
}

func WarnW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), WarnLevel, _defaultSkip, msg, m)
}

func ErrorW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), ErrorLevel, _defaultSkip, msg, m)
}

func FatalW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), FatalLevel, _defaultSkip, msg, m)
}

func PanicW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), PanicLevel, _defaultSkip, msg, m)
}

func DebugKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), DebugLevel, _defaultSkip, msg, keysAndValues)
}

func InfoKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), InfoLevel, _defaultSkip, msg, keysAndValues)
}

func WarnKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), WarnLevel, _defaultSkip, msg, keysAndValues)
}

func ErrorKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), ErrorLevel, _defaultSkip, msg, keysAndValues)
}

func FatalKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), FatalLevel, _defaultSkip, msg, keysAndValues)
}

func PanicKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), PanicLevel, _defaultSkip, msg, keysAndValues)
}

func Debug(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), DebugLevel, _defaultSkip, format, args...)
}

func Info(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), InfoLevel, _defaultSkip, format, args...)
}

func Warn(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), WarnLevel, _defaultSkip, format, args...)
}

func Error(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), ErrorLevel, _defaultSkip, format, args...)
}

func Panic(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), PanicLevel, _defaultSkip, format, args...)
}

func Fatal(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), FatalLevel, _defaultSkip, format, args...)
}

func (xLogger *XLogger) Debug(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), DebugLevel, _defaultSkip, format, args...)
}

func (xLogger *XLogger) Info(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), InfoLevel, _defaultSkip, format, args...)
}

func (xLogger *XLogger) Warn(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), WarnLevel, _defaultSkip, format, args...)
}

func (xLogger *XLogger) Error(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), ErrorLevel, _defaultSkip, format, args...)
}

func (xLogger *XLogger) Panic(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), PanicLevel, _defaultSkip, format, args...)
}

func (xLogger *XLogger) Fatal(format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), FatalLevel, _defaultSkip, format, args...)
}

func (xLogger *XLogger) DebugW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), DebugLevel, _defaultSkip, msg, m)
}

func (xLogger *XLogger) InfoW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), InfoLevel, _defaultSkip, msg, m)
}

func (xLogger *XLogger) WarnW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), WarnLevel, _defaultSkip, msg, m)
}

func (xLogger *XLogger) ErrorW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), ErrorLevel, _defaultSkip, msg, m)
}

func (xLogger *XLogger) FatalW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), FatalLevel, _defaultSkip, msg, m)
}

func (xLogger *XLogger) PanicW(msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), PanicLevel, _defaultSkip, msg, m)
}

func (xLogger *XLogger) DebugKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), DebugLevel, _defaultSkip, msg, keysAndValues)
}

func (xLogger *XLogger) InfoKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), InfoLevel, _defaultSkip, msg, keysAndValues)
}

func (xLogger *XLogger) WarnKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), WarnLevel, _defaultSkip, msg, keysAndValues)
}

func (xLogger *XLogger) ErrorKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), ErrorLevel, _defaultSkip, msg, keysAndValues)
}

func (xLogger *XLogger) FatalKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), FatalLevel, _defaultSkip, msg, keysAndValues)
}

func (xLogger *XLogger) PanicKV(msg string, keysAndValues []interface{}) {
	xLogger.h.LogWithKV(context.Background(), PanicLevel, _defaultSkip, msg, keysAndValues)
}

// DebugC with context logs a message.
func (xLogger *XLogger) DebugC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, DebugLevel, _defaultSkip, format, args...)
}

// InfoC with context logs a message.
func (xLogger *XLogger) InfoC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, InfoLevel, _defaultSkip, format, args...)
}

// WarnC with context logs a message.
func (xLogger *XLogger) WarnC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, WarnLevel, _defaultSkip, format, args...)
}

// ErrorC with context logs a message.
func (xLogger *XLogger) ErrorC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, ErrorLevel, _defaultSkip, format, args...)
}

// PanicC with context logs a message.
func (xLogger *XLogger) PanicC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, PanicLevel, _defaultSkip, format, args...)
}

// FatalC with context logs a message.
func (xLogger *XLogger) FatalC(ctx context.Context, format string, args ...interface{}) {
	xLogger.h.Log(ctx, FatalLevel, _defaultSkip, format, args...)
}
