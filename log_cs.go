package xlog

import "context"

func DebugCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, DebugLevel, skip, format, args...)
}

func InfoCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, InfoLevel, skip, format, args...)
}

func WarnCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, WarnLevel, skip, format, args...)
}

func ErrorCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, ErrorLevel, skip, format, args...)
}

func PanicCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, PanicLevel, skip, format, args...)
}

func FatalCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, FatalLevel, skip, format, args...)
}

func DebugWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), DebugLevel, skip, msg, m)
}

func InfoWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), InfoLevel, skip, msg, m)
}

func WarnWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), WarnLevel, skip, msg, m)
}

func ErrorWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), ErrorLevel, skip, msg, m)
}

func FatalWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), FatalLevel, skip, msg, m)
}

func PanicWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), PanicLevel, skip, msg, m)
}

func DebugS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), DebugLevel, skip, format, args...)
}

func InfoS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), InfoLevel, skip, format, args...)
}

func WarnS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), WarnLevel, skip, format, args...)
}

func ErrorS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), ErrorLevel, skip, format, args...)
}

func PanicS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), PanicLevel, skip, format, args...)
}

func FatalS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), FatalLevel, skip, format, args...)
}

func (xLogger *XLogger) DebugS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), DebugLevel, skip, format, args...)
}

func (xLogger *XLogger) InfoS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), InfoLevel, skip, format, args...)
}

func (xLogger *XLogger) WarnS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), WarnLevel, skip, format, args...)
}

func (xLogger *XLogger) ErrorS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), ErrorLevel, skip, format, args...)
}

func (xLogger *XLogger) PanicS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), PanicLevel, skip, format, args...)
}

func (xLogger *XLogger) FatalS(skip int, format string, args ...interface{}) {
	xLogger.h.Log(context.Background(), FatalLevel, skip, format, args...)
}

func (xLogger *XLogger) DebugWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), DebugLevel, skip, msg, m)
}

func (xLogger *XLogger) InfoWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), InfoLevel, skip, msg, m)
}

func (xLogger *XLogger) WarnWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), WarnLevel, skip, msg, m)
}

func (xLogger *XLogger) ErrorWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), ErrorLevel, skip, msg, m)
}

func (xLogger *XLogger) FatalWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), FatalLevel, skip, msg, m)
}

func (xLogger *XLogger) PanicWS(skip int, msg string, m map[string]interface{}) {
	xLogger.h.LogWith(context.Background(), PanicLevel, skip, msg, m)
}

func (xLogger *XLogger) DebugCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, DebugLevel, skip, format, args...)
}

func (xLogger *XLogger) InfoCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, InfoLevel, skip, format, args...)
}

func (xLogger *XLogger) WarnCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, WarnLevel, skip, format, args...)
}

func (xLogger *XLogger) ErrorCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, ErrorLevel, skip, format, args...)
}

func (xLogger *XLogger) PanicCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, PanicLevel, skip, format, args...)
}

func (xLogger *XLogger) FatalCS(ctx context.Context, skip int, format string, args ...interface{}) {
	xLogger.h.Log(ctx, FatalLevel, skip, format, args...)
}
