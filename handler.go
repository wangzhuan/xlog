package xlog

import "context"

// Handler is used to handle log events, outputting them to
// stdio or sending them to remote services.
type Handler interface {
	Log(context.Context, Level, int, string, ...interface{})
	LogWith(context.Context, Level, int, string, map[string]interface{})
	LogWithKV(context.Context, Level, int, string, []interface{})
	Close() error
}

// Handlers a bundle for handler with filter function.
type Handlers struct {
	filters  map[string]struct{}
	handlers []Handler
}

func NewHandlers(handlers ...Handler) *Handlers {
	//set := map[string]struct{}{}
	//for _, f := range filters {
	//	set[f] = struct{}{}
	//}
	return &Handlers{handlers: handlers}
}

func (hs *Handlers) Log(ctx context.Context, l Level, skip int, format string, args ...interface{}) {
	for _, h := range hs.handlers {
		h.Log(ctx, l, skip, format, args...)
	}
}

func (hs *Handlers) LogWith(ctx context.Context, l Level, skip int, msg string, m map[string]interface{}) {
	for _, h := range hs.handlers {
		h.LogWith(ctx, l, skip, msg, m)
	}
}

func (hs *Handlers) LogWithKV(ctx context.Context, l Level, skip int, msg string, keysAndValues []interface{}) {
	for _, h := range hs.handlers {
		h.LogWithKV(ctx, l, skip, msg, keysAndValues)
	}
}

func (hs *Handlers) Close() (err error) {
	for _, h := range hs.handlers {
		if err := h.Close(); err != nil {

		}
	}
	return
}
