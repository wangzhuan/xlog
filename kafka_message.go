package xlog

// kafkaMessage
// 写入kafka的消息结构，兼容filebeat结构
type kafkaMessage struct {
	Timestamp string                `json:"@timestamp"`
	Metadata  *KafkaMessageMetadata `json:"@metadata,omitempty"`
	Version   string                `json:"@version,omitempty"`
	Message   string                `json:"message"`
	Fields    KafkaMessageFields    `json:"fields"`
	Host      KafkaMessageHost      `json:"host"`
	Log       *KafkaMessageLog      `json:"log,omitempty"`
}

type KafkaMessageLog struct {
	File struct {
		Path string `json:"path"`
	} `json:"file"`
}

type KafkaMessageHost struct {
	Name string `json:"name"`
}

type KafkaMessageFields struct {
	Type    string `json:"type"`
	Project string `json:"project"`
}

type KafkaMessageMetadata struct {
	Beat    string `json:"beat"`
	Type    string `json:"type"`
	Version string `json:"version"`
	Topic   string `json:"topic"`
}
