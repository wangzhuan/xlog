package xlog

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"os"
	"time"

	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/compress"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var _hostName string

func init() {
	name, err := os.Hostname()
	if err != nil {
		fmt.Printf("[kafka_sink]--> fail_to_get_hostname, err: %v", err)
		return
	}
	_hostName = name
}

// CompressionType represents the compression applied to a record set.
type CompressionType = string

const (
	CompressionNone   CompressionType = ""
	CompressionGzip   CompressionType = "gzip"
	CompressionSnappy CompressionType = "snappy"
	CompressionLz4    CompressionType = "lz4"
	CompressionZstd   CompressionType = "zstd"
)

var _compressionTypeMap = map[CompressionType]compress.Compression{
	CompressionNone:   compress.None,
	CompressionGzip:   compress.Gzip,
	CompressionSnappy: compress.Snappy,
	CompressionLz4:    compress.Lz4,
	CompressionZstd:   compress.Zstd,
}

type KafkaConfig struct {
	// 元素配置参考："localhost:9092"
	AddressList   []string `mapstructure:"address_list" yaml:"address_list"`
	Topic         string   `mapstructure:"topic" yaml:"topic"`
	ServiceName   string   `mapstructure:"service_name" yaml:"service_name"`
	FilePathDepth int      `mapstructure:"file_path_depth" yaml:"file_path_depth"` // 日志中path字段记录的文件目录的层级深度
	LevelString   string   `mapstructure:"level_string" yaml:"level_string"`       // 输出的日志级别, 值：debug,info,warn,error,panic,fatal
	Console       bool     `mapstructure:"console" yaml:"console"`                 // 是否输出到控制台
	Async         *bool    `mapstructure:"async" yaml:"async"`                     // 是否异步调用

	// Limit the maximum size of a request in bytes before being sent to
	// a partition.
	//
	// The default is to use a kafka default value of 1048576.
	BatchBytes int64 `mapstructure:"batch_bytes" yaml:"batch_bytes"`

	// Limit on how many messages will be buffered before being sent to a
	// partition.
	//
	// The default is to use a target batch size of 100 messages.
	BatchSize int `mapstructure:"batch_size" yaml:"batch_size"`

	// 是否启用压缩，默认是空，可选类型为gzip、lz4、snappy、zstd四种。
	// 压缩会降低网络IO但是会增加生产者端的CPU消耗。
	// 另外如果broker端的压缩设置和生产者不同那么也会给broker带来重新解压缩和重新压缩的CPU负担。
	Compression CompressionType `mapstructure:"compression" yaml:"compression"`

	// Limit on how many attempts will be made to deliver a message.
	//
	// The default is to try at most 10 times.
	MaxAttempts int `mapstructure:"max_attempts" yaml:"max_attempts"`

	// Timeout for write operation performed by the Writer.
	//
	// Defaults to 10 seconds. 单位：秒
	WriteTimeout uint8 `mapstructure:"write_timeout" yaml:"write_timeout"`

	// 可选，默认会输出到本地，最多输出200M，1天有效期
	ErrorLogger Logger `mapstructure:"-" yaml:"-"`
}

func (p *KafkaConfig) validate() error {
	if p == nil {
		return errors.New("KafkaConfig is nil")
	}
	if p.Topic == "" {
		return errors.New("KafkaConfig.Topic is empty")
	}
	if len(p.AddressList) < 1 {
		return errors.New("KafkaConfig.AddressList is empty")
	}

	return nil
}

type kafkaSink struct {
	kafkaWriter *kafka.Writer
	config      *KafkaConfig
}

func configWithURL(kafkaURL *url.URL) (*KafkaConfig, error) {
	urlQuery := kafkaURL.Query()
	cfg := urlQuery.Get("cfg")
	if cfg == "" {
		return nil, errors.New("empty cfg")
	}

	var kafkaCfg KafkaConfig
	err := json.Unmarshal([]byte(cfg), &kafkaCfg)
	if err != nil {
		return nil, err
	}
	return &kafkaCfg, err
}

// KafkaSink
// kafkaURL 如: kafka://kafka?addr=ecs-hn1a-elk-kafka-1:9092,ecs-hn1a-elk-kafka-2:9092&topic=log_topic
// kafkaURL 的 queryString 需要做编码
// create kafka sink instance
func KafkaSink(kafkaURL *url.URL) (zap.Sink, error) {
	cfg, cfgErr := configWithURL(kafkaURL)
	if cfgErr != nil {
		return nil, cfgErr
	}
	// 默认1M int64(math.Pow(2, 20))
	batchBytes := int64(1048576)
	batchSize := 100
	if cfg.BatchBytes > 0 {
		batchBytes = cfg.BatchBytes
	}
	if cfg.BatchSize > 0 {
		batchSize = cfg.BatchSize
	}
	kafkaWriter := &kafka.Writer{
		Addr:         kafka.TCP(cfg.AddressList...),
		Topic:        cfg.Topic,
		Balancer:     &kafka.RoundRobin{},
		Async:        true,
		MaxAttempts:  cfg.MaxAttempts,
		BatchBytes:   batchBytes,
		BatchSize:    batchSize,
		RequiredAcks: kafka.RequireOne,
		WriteTimeout: time.Duration(cfg.WriteTimeout) * time.Second,
		ErrorLogger:  cfg.ErrorLogger,
	}

	if cfg.Async != nil {
		kafkaWriter.Async = *cfg.Async
	}
	if v, ok := _compressionTypeMap[cfg.Compression]; ok {
		kafkaWriter.Compression = v
	}
	kafkaWriter.Completion = func(messages []kafka.Message, err error) {
		if err != nil {
			fmt.Printf("[kafka_sink]--> kafka_completion_err: %v\n", err)
		}
	}
	if kafkaWriter.ErrorLogger == nil {
		kafkaWriter.ErrorLogger = NewKafkaSinkLogger()
	}

	kSink := kafkaSink{
		kafkaWriter: kafkaWriter,
		config:      cfg,
	}

	return &kSink, nil
}

// Close
// implement zap.Sink func Close
func (p *kafkaSink) Close() error {
	return p.kafkaWriter.Close()
}

// Write
// implement zap.Sink func Write
func (p *kafkaSink) Write(b []byte) (int, error) {
	timeOut := time.Duration(5)
	if p.config.WriteTimeout > 0 {
		timeOut = time.Duration(p.config.WriteTimeout)
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeOut*time.Second)
	defer cancel()

	timestampStr := time.Now().UTC().Format(time.RFC3339)
	kafkaWriter := p.kafkaWriter
	msg := kafkaMessage{
		Timestamp: timestampStr,
		Metadata:  nil,
		Version:   "",
		Message:   string(b),
		Fields: KafkaMessageFields{
			Type:    p.config.LevelString,
			Project: p.config.ServiceName,
		},
		Host: KafkaMessageHost{Name: _hostName},
		Log:  nil,
	}
	b1, bErr := json.Marshal(msg)
	if bErr != nil {
		fmt.Printf("[kafka_sink]--> fail_to_marshal_msg_err: %v \n", bErr)
		return 0, bErr
	}

	msgList := []kafka.Message{
		{
			Value: b1,
		},
	}
	var writeErr error

	switch err := kafkaWriter.WriteMessages(ctx, msgList...).(type) {
	case nil:
	case kafka.WriteErrors:
		for i := range msgList {
			if err[i] != nil {
				writeErr = err[i]
				fmt.Printf("[kafka_sink]--> write_err %v\n", err[i])
			}
		}
	default:
		writeErr = err
		fmt.Printf("[kafka_sink]--> write_err %v\n", err)
	}

	return len(b1), writeErr
}

// Sync
// implement zap.Sink func Sync
func (p *kafkaSink) Sync() error {
	return nil
}

type kafkaHandler struct {
	logger        *zap.SugaredLogger
	level         Level
	appId         string //服务名
	filePathDepth int
}

func (k *kafkaHandler) Log(ctx context.Context, l Level, skip int, format string, args ...interface{}) {
	if l < k.level {
		return
	}

	logger := k.getLogger(skip)

	traceID := ctx.Value(TraceIDHeaderKey)
	logger = logger.With(AppIdKey, k.appId, TraceIDKey, traceID)

	msg := format
	if msg == "" && len(args) > 0 {
		msg = fmt.Sprint(args...)
	} else if msg != "" && len(args) > 0 {
		msg = fmt.Sprintf(format, args...)
	}

	var keysAndValues []interface{}

	keysAndValues = append(keysAndValues, "msg")
	keysAndValues = append(keysAndValues, msg)

	log(logger, l, keysAndValues)
}

func (k *kafkaHandler) LogWith(ctx context.Context, l Level, skip int, msg string, m map[string]interface{}) {
	if l < k.level {
		return
	}

	logger := k.getLogger(skip)

	if msg != "" {
		m["msg"] = msg
	}
	if k.appId != "" {
		m[AppIdKey] = k.appId
	}

	var keysAndValues []interface{}

	for k, v := range m {
		keysAndValues = append(keysAndValues, k)
		keysAndValues = append(keysAndValues, v)
	}

	log(logger, l, keysAndValues)
}

func (k *kafkaHandler) LogWithKV(ctx context.Context, l Level, skip int, msg string, keysAndValues []interface{}) {
	if l < k.level {
		return
	}

	logger := k.getLogger(skip)

	if k.appId != "" {
		keysAndValues = append(keysAndValues, AppIdKey)
		keysAndValues = append(keysAndValues, k.appId)
	}

	if msg != "" {
		keysAndValues = append(keysAndValues, "msg")
		keysAndValues = append(keysAndValues, msg)
	}

	log(logger, l, keysAndValues)
}

func (k *kafkaHandler) Close() error {
	return nil
}

func (k *kafkaHandler) getLogger(skip int) (logger *zap.SugaredLogger) {
	baseSkip := 5
	s := baseSkip + skip
	logger = k.logger.With("path", getCaller(s, k.filePathDepth))
	return
}

func zapWriterWithConfig(kafkaConfig *KafkaConfig) (zapcore.WriteSyncer, error) {
	if err := zap.RegisterSink("kafka", KafkaSink); err != nil {
		return nil, err
	}
	vErr := kafkaConfig.validate()
	if vErr != nil {
		return nil, vErr
	}
	jBytes, mErr := json.Marshal(kafkaConfig)
	if mErr != nil {
		return nil, mErr
	}

	pathStr := fmt.Sprintf("cfg=%s", string(jBytes))
	pathStr = url.PathEscape(pathStr)
	sinkURLStr := fmt.Sprintf("kafka://kafka?%s", pathStr)
	zapWriter, _, zapErr := zap.Open(sinkURLStr)
	if zapErr != nil {
		return nil, zapErr
	}
	return zapWriter, nil
}

func NewKafkaHandler(kafkaConfig *KafkaConfig) (*kafkaHandler, error) {
	zapWriter, zErr := zapWriterWithConfig(kafkaConfig)
	if zErr != nil {
		return nil, zErr
	}

	// First, define our level-handling logic.
	//highPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
	//	return lvl >= zapcore.ErrorLevel
	//})
	//lowPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
	//	return lvl < zapcore.ErrorLevel
	//})

	xLogEncoder := defaultXLogEncoder()

	zapCores := make([]zapcore.Core, 0, 6)

	zapCores = append(zapCores, zapcore.NewCore(xLogEncoder, zapWriter, infoPriority))

	if kafkaConfig.Console {
		consoleDebugging := zapcore.Lock(os.Stdout)
		zapCores = append(zapCores, zapcore.NewCore(xLogEncoder, consoleDebugging, infoPriority))
	}

	core := zapcore.NewTee(zapCores...)

	appID := ""
	if kafkaConfig.ServiceName != "" {
		appID = kafkaConfig.ServiceName
	}

	// From a zapcore.Core, it's easy to construct a Logger.
	logger := zap.New(core).Sugar()
	defer logger.Sync()

	handler := kafkaHandler{
		logger:        logger,
		level:         LevelStringToCode(kafkaConfig.LevelString),
		appId:         appID,
		filePathDepth: kafkaConfig.FilePathDepth,
	}
	return &handler, nil
}
